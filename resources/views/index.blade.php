<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/master.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway|Roboto:500" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
    <script type="text/javascript" src="js/master.js"></script>
  </head>
  <body >
    <div class="row fix fix">
    <div class="col-lg-3 col-md-5 col-md-offset-1 col-sm-6 col-xs-6" id="logo">
      <h1><a href="/project_yi/public/products">|EXPENSIVE|</a></h1>
    </div>
    <div id="menu" class="col-lg-4 col-md-5 col-sm-6 col-xs-6">
      <ul>
        <li>HELP</li>
        <li>CONTACT US</li>
        <li>GB - £GBP</li>
      </ul>
    </div>
    <div class="col-lg-3 col-md-12 col-sx-12" id="search">
      <input type="text" name="" value="" >
      <button type="button" name="search" >Search</button>
    </div>
  </div>
    <div class="row fix fix" id="page_break" style="border-bottom:solid 2.5px lightgrey;">
      <nav class="col-lg-6 col-lg-offset-2 col-md-6 col-sm-6 col-xs-12">
        <ul>
          <li>MAN</li>
          <li>WOMAN</li>
          <li>BEAUTY</li>
          <li>HOMEWARE</li>
          <li>LIFE</li>
          <li>OUTLET</li>
        </ul>
      </nav>
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
        <ul>
          <li>MY ACOUNT</li>
          <li>MY BAG</li>
        </ul>
      </div>
    </div>

    <div class="row fix">
      <div class="col-lg-3 col-lg-offset-2" id="path" style="font-size: 12px;">
        <p>Home / Mens / Category / Jackets</p>
      </div>
    </div>

    <div class="row fix" id="index_product_list">
      <div class="col-lg-2  col-lg-offset-2 " >
        <button id="filter_btn" type="button" name="button">Filter</button>
        <div id="refine_boxes">
          <b>Filter</b>
          <div class="col-lg-12" >
            <b>Clothing Style</b>
            <hr>
            <div class="scroll_box">
              <input type="checkbox" name="" value=""><label for="">Biker and Bomber Jackets</label><br>
              <input type="checkbox" name="" value=""><label for="">Blazer</label><br>
              <input type="checkbox" name="" value=""><label for="">Casual Jackets</label><br>
              <input type="checkbox" name="" value=""><label for="">Coats</label><br>
              <input type="checkbox" name="" value=""><label for="">Denim Jackets</label><br>
              <input type="checkbox" name="" value=""><label for="">Gilets</label><br>
              <input type="checkbox" name="" value=""><label for="">Leather Jackets</label><br>
              <input type="checkbox" name="" value=""><label for="">Lightweight Jackets</label><br>
              <input type="checkbox" name="" value=""><label for="">Padded Jackets</label><br>

            </div>
          </div>

          <div class="col-lg-12" >
            <b>Brand</b>
            <hr>
            <div class="scroll_box">
              <input type="checkbox" name="" value=""><label for="">APC</label><br>
              <input type="checkbox" name="" value=""><label for="">Alexander Wang</label><br>
              <input type="checkbox" name="" value=""><label for="">Barbour</label><br>
              <input type="checkbox" name="" value=""><label for="">Barbour X Steve</label><br>
              <input type="checkbox" name="" value=""><label for="">Cavern</label><br>
              <input type="checkbox" name="" value=""><label for="">Champion</label><br>
              <input type="checkbox" name="" value=""><label for="">Folk</label><br>
              <input type="checkbox" name="" value=""><label for="">Gant Rugger</label><br>
              <input type="checkbox" name="" value=""><label for="">Helmut Lang</label><br>
              <input type="checkbox" name="" value=""><label for="">HUGO</label><br>
            </div>
          </div>

          <div class="col-lg-12" >
            <b>Colour Range</b>
            <hr>
            <div class="scroll_box">
              <input type="checkbox" name="" value=""><label for="">Beige</label><br>
              <input type="checkbox" name="" value=""><label for="">Black</label><br>
              <input type="checkbox" name="" value=""><label for="">Blue</label><br>
              <input type="checkbox" name="" value=""><label for="">Green</label><br>
              <input type="checkbox" name="" value=""><label for="">Grey</label><br>
              <input type="checkbox" name="" value=""><label for="">Multi</label><br>
              <input type="checkbox" name="" value=""><label for="">Pink</label><br>
              <input type="checkbox" name="" value=""><label for="">Stone</label><br>
            </div>
          </div>

          <div class="col-lg-12" >
            <b>Clothing Size</b>
            <hr>
            <div class="scroll_box">
              <input type="checkbox" name="" value=""><label for="">XS</label><br>
              <input type="checkbox" name="" value=""><label for="">S</label><br>
              <input type="checkbox" name="" value=""><label for="">M</label><br>
              <input type="checkbox" name="" value=""><label for="">L</label><br>
              <input type="checkbox" name="" value=""><label for="">XL</label><br>
              <input type="checkbox" name="" value=""><label for="">XLL</label><br>
            </div>
          </div>
        </div>
        </div>


      <div class="col-lg-6 ">
        <p style="font-size: 20px;">Jackets</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
        <br>
        <div class="row" style="margin-bottom:30px;">
          <div class="col-lg-1">
              <p style="margin:10px 0 0 0">Sort by</p>
          </div>
          <div class="col-lg-1">
            <select>
              <option value="default">Default</option>
              <option value="popularity">Popularity</option>
              <option value="low-high">Price: Low to High</option>
              <option value="high-low">Price: High to Low</option>
              <option value="a-z">A - Z</option>

            </select>
          </div>
        </div>


        @if(isset($clothes))
                @foreach($clothes as $clothing)
                <div class="col-md-4" style="text-align:center;" id="index_product" >
                  <a href="/project_yi/public/products/{{$clothing->id}}"><img src="{{$clothing->url}}" alt=""></a>
                  <hr>
                  <div class="col-md-12">
                    <b>{{$clothing->brand}}</b>
                  </div>
                  <div class="col-md-12 box">
                    <p>{{$clothing->name}}</p>
                  </div>
                  <div class="col-md-12 price">
                    <b>£{{$clothing->price}}</b>
                  </div>

                </div>

                @endforeach
              @else
                <p>No clothes here</p>
              @endif

              </div>

    </div>

    <div class="row fix footer_things" >

      <div class="col-lg-2 col-lg-offset-2 col-md-3 col-md-offset-1 col-sm-2 col-sm-offset-2" >
        <p>ABOUT EXPENSIVE</p>
        <ul>
          <li style="display:block;">Affiliates and Partners</li>
          <li style="display:block;">Our Story</li>
          <li style="display:block;">Expensive life</li>
          <li style="display:block;">Stockists</li>
        </ul>
      </div>

      <div class="col-lg-2 col-md-2 col-sm-2">
        <p>CUSTOMER SERVICES</p>
        <ul>
          <li style="display:block;">Contact Us</li>
          <li style="display:block;">Delivery Information</li>
          <li style="display:block;">Special Offer Exclusions</li>
          <li style="display:block;">Help</li>
        </ul>
      </div>

      <div class="col-lg-2 col-md-2 col-sm-2">
        <p>TERMS AND CONDITIONS</p>
        <ul>
          <li style="display:block;">Cookie Information</li>
          <li style="display:block;">Privacy Policy</li>
          <li style="display:block;">Returns Policy</li>
          <li style="display:block;">Terms and Conditions</li>
        </ul>
      </div>

      <div class="col-lg-2 col-md-2 col-sm-2">
        <p>HOW TO CONTACT US</p>
        <ul>
          <li style="display:block;">Monday - Friday: 8AM to 8PM</li>
          <li style="display:block;">Saturday: 9AM to 4PM</li>
          <li style="display:block;">Sunday: Closed</li>
          <li style="display:block;">Telephone: 01618 131 497</li>
        </ul>
      </div>
    </div>

    <footer>Powered by gainzzzz</footer>

  </body>
</html>
