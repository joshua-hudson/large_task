<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{{$jacket->name}}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/master.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway|Roboto:500" rel="stylesheet">
  </head>
  <body>
    <div class="row fix fix">
    <div class="col-lg-3 col-md-5 col-md-offset-1 col-sm-12 col-xs-12" id="logo">
      <h1><a href="/project_yi/public/products">|EXPENSIVE|</a></h1>
    </div>
    <div id="menu" class="col-lg-4 col-md-5 col-sm-6 col-xs-6">
      <ul>
        <li>HELP</li>
        <li>CONTACT US</li>
        <li>GB - £GBP</li>
      </ul>
    </div>
    <div class="col-lg-3 col-md-12 col-sx-12" id="search">
      <input type="text" name="" value="" >
      <button type="button" name="search" >Search</button>
    </div>
  </div>
    <div class="row fix fix" id="page_break" style="border-bottom:solid 2.5px lightgrey;">
      <nav class="col-lg-6 col-lg-offset-2 col-md-6 col-sm-6 col-xs-12">
        <ul>
          <li>MAN</li>
          <li>WOMAN</li>
          <li>BEAUTY</li>
          <li>HOMEWARE</li>
          <li>LIFE</li>
          <li>OUTLET</li>
        </ul>
      </nav>
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
        <ul>
          <li>MY ACOUNT</li>
          <li>MY BAG</li>
        </ul>
      </div>
    </div>

    <!-- START OF THE CONTENT -->

      <div class="row fix fix" >

        <!--START OF THE LEFT SIDE OF THE CONTENT -->
        <div class="col-lg-4 col-lg-offset-2 col-md-6 col-sm-12 col-xs-12" style="border-right:solid 1px lightgrey;">
          <div id="big_pic" class="col-xs-6 col-xs-offset-3">
            <img class="" src="../{{$jacket->url}}" alt="">
          </div>
          <img id="large_big_pic" class="" src="../{{$jacket->url}}" alt="">

          <div class="row fix">
            <div class="col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-1 thumbnail mini_pics">
              <img class="" src="../{{$jacket->url}}" alt="">
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2 thumbnail mini_pics">
              <img class="" src="../{{$jacket->url}}" alt="">
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2  thumbnail mini_pics">
              <img class="" src="../{{$jacket->url}}" alt="">
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2  thumbnail mini_pics">
              <img class="" src="../{{$jacket->url}}" alt="">
            </div>
          </div>

          <div id="mobile_add_basket" class="">
            <div class="row fix">
              <div class="col-xs-8 col-xs-offset-2">
                <p for="color" style="margin-top:10px;">Colour:</p>
              </div>
              <div class="col-xs-8 col-xs-offset-2">
                <select class="" name="color">
                  <option value="{{$jacket->colour}}">{{$jacket->colour}}</option>
                </select>
              </div>
             </div>

            <div class="row fix" style="margin-top:5px;">
              <div class="col-lg-2">
                <p for="color" style="margin-top:10px;">Size:</p>
              </div>
              <div class="col-xs-8 col-xs-offset-2">
                <select class="" name="sizes">
                  <option value="X-Small">X-Small</option>
                  <option value="Small">Small</option>
                  <option value="Medium">Medium</option>
                  <option value="Large">Large</option>
                  <option value="Extra-Large">Extra-Large</option>
                </select>
              </div>
             </div>

             <div class="row fix">
               <div class="col-xs-12">
                 <p style="padding:10px;">QTY:</p>
               </div>
               <div class="col-xs-12">
                 <input type="text" name="" value="" placeholder="1" style="width:40px; height:40px; text-align:center;">
               </div>
               <div class="col-xs-4 col-xs-offset-4">
                 <button type="submit" name="add" class="btn" >Add to Basket</button>
               </div>
             </div>
             <hr>
          </div>

          <div class="row fix" style="margin-bottom: 40px;">
            <div id="description" class="col-lg-12 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-6 col-xs-offset-3">
              <b>Description</b>
              <hr>
              <p>{{$jacket->description}}</p>
              <p>{{$jacket->material}}</p>
            </div>
          </div>

          <div class="row fix">
            <div id="product_details" class="col-lg-12 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-6 col-xs-offset-3">
              <b>Product Detials</b>
              <hr>
              <p><strong>Brand: </strong> {{$jacket->brand}}</p>
              <p><strong>Colour: </strong> {{$jacket->colour}}</p>
              <p><strong>Material: </strong> {{$jacket->material}}</p>
              <p><strong>Clothing Style: </strong> {{$jacket->clothing_style}}</p>
            </div>
          </div>

        </div>

        <div id="right_side_bar" class="col-lg-4 col-md-5 col-sm-6 col-xs-0">
          <div class="row fix">
            <div class="col-lg-12">
              <h3>{{$jacket->brand}}</h1>
            </div>
            <div class="col-lg-12">
              <p> {{$jacket->name}}</p>
            </div>
            <div class="col-lg-12">
              <h3>£{{$jacket->price}}</h3>
            </div>
          </div>

          <div class="row fix">
            <div class="col-lg-2">
              <p for="color" style="margin-top:10px;">Colour:</p>
            </div>
            <div class="col-lg-10">
              <select class="" name="color">
                <option value="{{$jacket->colour}}">{{$jacket->colour}}</option>
              </select>
            </div>
           </div>

           <div class="row fix" style="margin-top:5px;">
             <div class="col-lg-2">
               <p for="color" style="margin-top:10px;">Size:</p>
             </div>
             <div class="col-lg-10">
               <select class="" name="sizes">
                 <option value="X-Small">X-Small</option>
                 <option value="Small">Small</option>
                 <option value="Medium">Medium</option>
                 <option value="Large">Large</option>
                 <option value="Extra-Large">Extra-Large</option>
               </select>
             </div>
            </div>

            <hr>
            <div class="row fix">
              <div class="col-lg-2 col-sm-2">
                <p style="padding:10px;">QTY:</p>
              </div>
              <div class="col-lg-2 col-sm-2">
                <input type="text" name="" value="" placeholder="1" style="width:40px; height:40px; text-align:center;">
              </div>
              <div class="col-lg-8 col-sm-8">
                <button type="submit" name="add" class="btn" style="height:40px;">Add to Basket</button>
              </div>
            </div>
            <hr>
            <div class="row fix">
              <div class="col-lg-12 col-sm-12">
                <h3>Style with</h3>
                <div class="col-lg-4 col-md-4 col-sm-4" style="text-align:center;">
                  @foreach($shirts as $shirt)
                  @if($shirt->outfit == $jacket->outfit)
                  <img class="thumbnail" src="../{{$shirt->url}}" alt="">
                  <b>{{$shirt->brand}}</b>
                  <p style="height: 80px;">{{$shirt->name}}</p>
                  <b>£{{$shirt->price}}</b> <br>
                  <button type="button" name="button" class="btn ">Add to Basket</button>
                  @endif
                  @endforeach
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4" style="text-align:center;">
                  @foreach($pants as $pant)
                  @if($pant->outfit == $jacket->outfit)
                  <img class="thumbnail" src="../{{$pant->url}}" alt="">
                  <b>{{$pant->brand}}</b>
                  <p style="height: 80px;">{{$pant->name}}</p>
                  <b>£{{$pant->price}}</b> <br>
                  <button type="button" name="button" class="btn ">Add to Basket</button>
                  @endif
                  @endforeach
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4" style="text-align:center;">
                  @foreach($shoes as $shoe)
                  @if($shoe->outfit == $jacket->outfit)
                  <img class="thumbnail" src="../{{$shoe->url}}" alt="">
                  <b>{{$shoe->brand}}</b>
                  <p style="height: 80px;">{{$shoe->name}}</p>
                  <b>£{{$shoe->price}}</b> <br>
                  <button type="button" name="button" class="btn ">Add to Basket</button>
                  @endif
                  @endforeach
                </div>


              </div>


            </div>

            <div class="row fix" style="margin-top:20px;">
              <div class="col-lg-11">
                <b>Delivery Information</b>
                <hr>
                <p>UK next day delivery available until 9pm plus over 160 countries shipped to worldwide.</p>
              </div>
            </div>

            <div class="row fix" style="margin-top:20px;">
              <div class="col-lg-11">
                <b>Returns Policy</b>
                <hr>
                <p>We are confident that you will be happy with your purchase. However, if you are not satisfied you can return your item for a full refund.</p>
              </div>
            </div>



          </div>

          <div id="mobile_style_with" class="row fix">
            <div class="col-xs-12 mobile_style_with">
              <hr>
              <div class="col-xs-4 mobile_style_with_img" style="text-align:center;">
                @foreach($shirts as $shirt)
                @if($shirt->outfit == $jacket->outfit)
                <div class="row fix">
                  <img class="" src="../{{$shirt->url}}" alt="">
                </div>
                <b>{{$shirt->brand}}</b>
                <p style="height: 4em;">{{$shirt->name}}</p>
                <b>£{{$shirt->price}}</b> <br>
                <button type="button" name="button" class="btn ">Add to Basket</button>
                @endif
                @endforeach
              </div>

              <div class="col-xs-4 mobile_style_with_img" style="text-align:center;">
                @foreach($pants as $pant)
                @if($pant->outfit == $jacket->outfit)
                <div class="row fix">
                  <img class="" src="../{{$pant->url}}" alt="">
                </div>
                <b>{{$pant->brand}}</b>
                <p style="height: 4em;">{{$pant->name}}</p>
                <b>£{{$pant->price}}</b> <br>
                <button type="button" name="button" class="btn ">Add to Basket</button>
                @endif
                @endforeach
              </div>

              <div class="col-xs-4 mobile_style_with_img" style="text-align:center;">
                @foreach($shoes as $shoe)
                @if($shoe->outfit == $jacket->outfit)
                <div class="row fix">
                  <img class="" src="../{{$shoe->url}}" alt="">
                </div>
                <b>{{$shoe->brand}}</b>
                <p style="height: 4em;">{{$shoe->name}}</p>
                <b>£{{$shoe->price}}</b> <br>
                <button type="button" name="button" class="btn ">Add to Basket</button>
                @endif
                @endforeach
              </div>
            </div>
          </div>




      </div>

      <div id="prize" class="row fix">
        <div class="col-lg-6 col-lg-offset-3" style="text-align:center;">
          <h3>Customer Review</h3>
          <hr>
          <p>There are currently no reviews.</p>
          <button type="button" name="button" class="btn">Create a Review</button>
          <br>
          <p>Write a review to be in a change to win £100</p>
        </div>
      </div>


    </div>

    <div class="row fix fix footer_things"  >

      <div class="col-lg-2 col-lg-offset-2 col-md-3 col-sm-3 ">
        <p>ABOUT EXPENSIVE</p>
        <ul>
          <li style="display:block;">Affiliates and Partners</li>
          <li style="display:block;">Our Story</li>
          <li style="display:block;">Expensive life</li>
          <li style="display:block;">Stockists</li>
        </ul>
      </div>

      <div class="col-lg-2 col-md-3 col-sm-3">
        <p>CUSTOMER SERVICES</p>
        <ul>
          <li style="display:block;">Contact Us</li>
          <li style="display:block;">Delivery Information</li>
          <li style="display:block;">Special Offer Exclusions</li>
          <li style="display:block;">Help</li>
        </ul>
      </div>

      <div class="col-lg-2 col-md-3 col-sm-3">
        <p>TERMS AND CONDITIONS</p>
        <ul>
          <li style="display:block;">Cookie Information</li>
          <li style="display:block;">Privacy Policy</li>
          <li style="display:block;">Returns Policy</li>
          <li style="display:block;">Terms and Conditions</li>
        </ul>
      </div>

      <div class="col-lg-2 col-md-3 col-sm-3">
        <p>HOW TO CONTACT US</p>
        <ul>
          <li style="display:block;">Monday - Friday: 8AM to 8PM</li>
          <li style="display:block;">Saturday: 9AM to 4PM</li>
          <li style="display:block;">Sunday: Closed</li>
          <li style="display:block;">Telephone: 01618 131 497</li>
        </ul>
      </div>
    </div>

  </body>

  <footer class="fix">

  </footer>
</html>
