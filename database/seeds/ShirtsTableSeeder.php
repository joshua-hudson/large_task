<?php

use Illuminate\Database\Seeder;

class ShirtsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('shirts')->insert([
        ['id' => 1, 'brand' => 'Folk', 'name' => "Folk Men's Collarless Shirt - White", 'description' => "Folk white collarless shirt with a concealed button placket and one exposed corozo button to the top. Crafted from pure cotton, the men’s collarless shirt is finished with a curved hem and two-button adjustable cuffs.", 'material' => '100% Cotton', 'colour' => 'White', 'style' => 'Long Sleeved Shirts', 'url' => '/img/shirts/folk_mens_collarless_shirt.jpg', 'price' => '125.00', 'outfit' => '1'],

        ['id' => 2, 'brand' => 'A.P.C.', 'name' => "A.P.C. Men's Chemise Saturday Shirt - Bleu Fonce", 'description' => "Men’s blue ‘Saturday’ chemise check shirt from French label A.P.C. with ribbed ‘V’ inset below the neckline. Crafted from soft cotton poplin with a woven design, the shirt is detailed with a chest pocket, corozo buttons and a pointed collar. Complete with a curved hem, gusseted sides and button cuffs.", 'material' => '100% Cotton', 'colour' => 'Blue', 'style' => 'Long Sleeved Shirts', 'url' => '/img/shirts/apc_mens_chemise_saturday_shirt.jpg', 'price' => '72.00', 'outfit' => '2'],

        ['id' => 3, 'brand' => 'KENZO', 'name' => "KENZO Men's Cartoon Print Short Sleeve Shirt - White", 'description' => "Men’s ‘Bermudas’ white short sleeve shirt with an all over cartoon flyer print pattern from KENZO. With graphics inspired by 90s NYC nightlife, the slim fit cotton poplin shirt features a chest patch pocket and a classic pointed collar. Complete with KENZO engraved buttons and a curved hem.", 'material' => '100% Cotton', 'colour' => 'White', 'style' => 'Short Sleeved Shirts', 'url' => '/img/shirts/kenzo_mens_cartoon_print_short_sleeve_shirt.jpg', 'price' => '135.00', 'outfit' => '3'],

        ['id' => 4, 'brand' => 'HUGO', 'name' => "HUGO Men's Esid Collar Detail Shirt - Open White", 'description' => "Slim fit white shirt from HUGO. With a pure cotton construction, the 'Esid' shirt comprises a Kent collar with pleat detailing, a concealed button front placket and long sleeves with adjustable buttoned cuffs. A back yoke and curved hem also feature. Complete with rear shaping darts and tonal stitching throughout.", 'material' => '100% Cotton', 'colour' => 'White', 'style' => 'Long Sleeved Shirts', 'url' => '/img/shirts/hugo_mens_esid_collar_detail_shirt.jpg', 'price' => '95.00', 'outfit' => '4'],

        ['id' => 5, 'brand' => 'Polo Ralph Lauren', 'name' => "Polo Ralph Lauren Men's Custom Fit Button Down Pinpoint Oxford Shirt - Blue", 'description' => "Custom fit Oxford shirt from Polo Ralph Lauren. Crafted from pure cotton and cut for a slim fit, the blue shirt comprises a button down collar, full front button placket and long sleeves with buttoned cuffs. The rear features a curved hem and a curved back yoke finished with a box pleat. The ‘Pinpoint’ shirt is complete with the signature pony logo stitched to the chest.", 'material' => '100% Cotton', 'colour' => 'Blue', 'style' => 'Long Sleeved Shirts', 'url' => '/img/shirts/polo_ralph_lauren_mens_custom_fit_button_down_pinpoint_oxford_shirt.jpg', 'price' => '60.00', 'outfit' => '5'],

        ['id' => 6, 'brand' => 'KENZO', 'name' => "KENZO Men's Painted Logo Shirt - White", 'description' => "Men's long sleeve shirt from KENZO. Cut from lightweight cotton, the shirt features a pointed collar, central button placket and large, painted-effect KENZO Paris logo across the front. Complete with long sleeves, buttoned cuffs and a curved hem. Straight hem.", 'material' => '100% Cotton', 'colour' => 'White', 'style' => 'Long Sleeved Shirts', 'url' => '/img/shirts/kenzo_mens_painted_logo_shirt.jpg', 'price' => '135.00', 'outfit' => '6'],


        ]);
    }
}
