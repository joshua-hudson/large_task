<?php

use Illuminate\Database\Seeder;

class ClothingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('clothings')->insert([
            ['id' => 1, 'brand' => 'Helmut Lang', 'name' => "Helmut Lang Men's Sherpa Bomber Jacket - Nomad", 'description' => "Men's 'Sherpa' bomber jacket from Helmut Lang with a shearling-inspired construction. Cut in an oversized fit, the lined jacket comprises a baseball-style collar, dual front zip closure and two snap fasten flap pockets. Complete with ribbed leather trims.", 'material' => 'Outer: 100% Polyester. Trims: 100% Lamb Leather.', 'colour' => 'Stone', 'clothing_style' => 'Biker and Bomber Jackets', 'url' => '/img/jackets/helmut_lang_mens_sherpa_bomber_jacket_nomad.jpg', 'price' => '790.00', 'outfit' => '1'],
            ['id' => 2, 'brand' => 'A.P.C', 'name' => "A.P.C. Men's Veste Duckie Suit Jacket - Dark Navy", 'description' => "Men's smart suit jacket from A.P.C. with Italian cotton tricotine construction and single breasted design. Cutting a regular fit, the 'Duckie' jacket features a tailored collar with notch lapels and decorative buttonhole detail, a two button fastening and a welt pocket to the chest. Fully lined, the jacket is complete with two patch pockets to the hips, darts to the front and buttoned cuffs. Piped inside pocket. Double vent to the rear.", 'material' => '100% Cotton.', 'colour' => 'Dark Navy', 'clothing_style' => 'Blazers', 'url' => '/img/jackets/apc_mens_veste_duckie_suit_jacket.jpg', 'price' => '435.00', 'outfit' => '2'],
            ['id' => 3, 'brand' => 'Michael Kors', 'name' => "Michael Kors Men's Ea1 Hybrid Bomber Jacket - Midnight", 'description' => "Men’s ‘Ea1 Hybrid’ bomber jacket from Michael Kors. Forming an iconic bomber silhouette, the jacket is constructed from a durable shell fibre with a high neck funnel collar and cuffed sleeves with classic ribbed knit trims. Secured with a zip and popper fastening to the front, the style offers side pockets as well as two flap utility pockets to the chest. Featuring an internal zipped pocket and finished with a branded metal tab to the nape. ", 'material' => '100% Polyester.', 'colour' => 'Dark Navy', 'clothing_style' => 'Biker and Bomber Jackets', 'url' => '/img/jackets/michael_kors_mens_ea1_hybrid_bomber_jacket.jpg', 'price' => '156.00', 'outfit' => '3'],
            ['id' => 4, 'brand' => 'Vivienne Westwood MAN', 'name' => "Vivienne Westwood MAN Men's Washed Cotton Bomber Jacket - Grey", 'description' => "Men’s grey washed cotton bomber jacket from Vivienne Westwood MAN. Crafted from textured cotton cloth, the jacket features a chunky gold-tone, dual zip fastening, two snap fastened flap pockets and a signature Vivienne Westwood gold orb logo embroidered on the chest. Fully lined, the jacket is finished with elasticated cuffs and waistband, and an internal chest pocket.", 'material' => '100% Cotton.', 'colour' => 'Grey', 'clothing_style' => 'Biker and Bomber Jackets', 'url' => '/img/jackets/vivienne_westwood_man_mens_washed_cotton_bomber_jacket.jpg', 'price' => '273.00', 'outfit' => '4'],
            ['id' => 5, 'brand' => 'A.P.C', 'name' => "A.P.C. Men's Blouson Alain Jacket - Dark Navy", 'description' => "A.P.C. navy ‘Alain’ bomber jacket cut from Italian cotton-linen blend tricotine with a waterproof finish. Fastened with a central zip closure, the jacket is detailed with a protective topstitched flap under the zip, raglan pockets with wide piped trims and a patch pocket on the left seam. The inside of the jacket is lined with a cotton blend fabric and features a piped vertical pocket. Complete with silver-tone hardware, a stand-up collar with elasticated ribbing and ribbed trims.", 'material' => '80% Cotton, 20% Linen', 'colour' => 'Navy', 'clothing_style' => 'Biker and Bomber Jackets', 'url' => '/img/jackets/apc_mens_blouson_alain_jacket.jpg', 'price' => '336.00', 'outfit' => '5'],
            ['id' => 6, 'brand' => 'Folk', 'name' => "Folk Men's Bomber Collar Jacket - Navy", 'description' => "Men’s navy bomber jacket with a snap fastened front closure from Folk. Crafted from soft loopback cotton jersey, the bomber jacket is detailed with side welt pockets, a classic ribbed bomber collar and a drawstring adjustable hem. Complete with a signature ‘Folk’ brand patch to the front.", 'material' => '100% Cotton', 'colour' => 'Navy', 'clothing_style' => 'Biker and Bomber Jackets', 'url' => '/img/jackets/folk_mens_bomber_collar_jacket.jpg', 'price' => '93.00', 'outfit' => '6'],

        ]);
    }
}
