<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //$this->call(UsersTableSeeder::class);
      $this->call(ClothingTableSeeder::class);
      $this->call(PantsTableSeeder::class);
      $this->call(ShoesTableSeeder::class);
      $this->call(ShirtsTableSeeder::class);
    }
}
