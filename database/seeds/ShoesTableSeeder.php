<?php

use Illuminate\Database\Seeder;

class ShoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('shoes')->insert([
        ['id' => 1, 'brand' => 'Vans', 'name' => "Vans Men's Sk8-Hi Canvas Hi-Top Trainers - Grape Leaf", 'description' => "Men’s ‘Sk8-Hi’ trainers from Vans. Crafted from durable canvas, the hi-top sneakers comprise a rounded toe and a lace up fastening with tonal eyelets and contrasting, white laces. Sitting at the ankle, the trainers boast a padded collar for support and comfort.", 'material' => 'Upper: Canvas. Sole: Rubber.', 'colour' => 'Green', 'style' => 'High Top Trainers', 'url' => '/img/shoes/vans_mens_sk8hi_canvas_hitop_trainers.jpg', 'price' => '57.00', 'outfit' => '1'],

        ['id' => 2, 'brand' => 'Dr. Martens', 'name' => "Dr. Martens Men's 3989 Original Archives Smooth Wingtip Brogues - Black", 'description' => "Dr. Martens '3989' black leather brogues from the Lost Archives collection. Originally released in the mid-60s, the leather brogues feature punched longwing detailing that runs the length of the shoe and a classic wing tip toe. Crafted with a traditional Goodyear welt, the men's shoes feature a chunky black, air-cushioned PVC sole that provides comfort and slip resistance. Complete with signature yellow stitching.", 'material' => 'Upper: Leather. Sole: PVC.', 'colour' => 'Black', 'style' => 'Brogues', 'url' => '/img/shoes/drmartins_mens_3989_original_archives_smooth_wingtip_brogues.jpg', 'price' => '105.00', 'outfit' => '2'],


        ['id' => 3, 'brand' => 'KENZO', 'name' => "KENZO Men's Stanley Kenzo Stripes Canvas Espadrilles - Black", 'description' => "Men's striped espadrilles with lightweight canvas uppers and a navy woven jute midsole from KENZO. The slip-on shoes are reinforced with stitching to the toe and outsole, and feature a signature KENZO Paris logo to the front. Complete with a rubber sole for added grip and a woven ‘Paris’ heel pull tab.", 'material' => 'Upper: Cotton. Sole: Rubber.', 'colour' => 'Black', 'style' => 'Espadrilles', 'url' => '/img/shoes/kenzo_mens_stanley_kenzo_stripes_canvas_espadrilles.jpg', 'price' => '£175.00', 'outfit' => '3'],


        ['id' => 4, 'brand' => 'Lacoste', 'name' => "Lacoste Men's Gazon BL 1 Perforated Leather Slip-On Trainers - Black", 'description' => "Men’s slip-on trainers from Lacoste. Crafted from smooth and perforated leather, the black ‘Gazon BL 1’ pumps comprise a slip-on design with a rounded toe and elasticated side panels for easy on and off. Placed upon a contrasting rubber sole with a breathable Ortholite insole, the trainers are complete with the signature green crocodile adorned to the side.", 'material' => 'Upper: Leather. Insole: Ortholite. Sole: Rubber.', 'colour' => 'Black', 'style' => 'Low Top Trainers', 'url' => '/img/shoes/lacoste_mens_gazon_bl_1_perforated_leather_slipon_trainers.jpg', 'price' => '65.00', 'outfit' => '4'],

        ['id' => 5, 'brand' => 'Lacoste', 'name' => "Lacoste Men's L.30 Slide 117 2 Slide Sandals - Navy", 'description' => "Men’s 'L.30 Slide 117 2' slide sandals from Lacoste. Crafted from water-resistant fabric printed with branding, the sandals comprise a wide, padded strap and are placed upon a contrasting footbed with carefully designed grooves for water drainage. Featuring a vulcanised rubber sole, the slides are complete with the signature, green crocodile logo adorned to the side.", 'material' => 'Upper: Synthetic. Sole: Rubber.', 'colour' => 'Navy', 'style' => 'Slide Sandals', 'url' => '/img/shoes/lacoste_mens_l30_slide_117_2_slide_sandals.jpg', 'price' => '35.00', 'outfit' => '5'],

        ['id' => 6, 'brand' => 'Grenson', 'name' => "Grenson Men's Nolan Leather Chelsea Boots - Black", 'description' => "Men's ‘Nolan’ black leather Chelsea boots from heritage British shoemakers Grenson. Crafted from smooth calf leather, the classic slip-on boots feature V-shaped elasticated side gores and heel loops. Set on a tonal black leather sole with a small heel, the boots are complete with smooth leather linings and slightly cushioned footbeds.", 'material' => 'Upper: Leather. Sole: Leather.', 'colour' => 'Black', 'style' => 'Chelsea Boots', 'url' => '/img/shoes/grenson_mens_nolan_leather_chelsea_boots.jpg', 'price' => '220.00', 'outfit' => '6']
        ]);

    }
}
