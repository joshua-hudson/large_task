<?php

use Illuminate\Database\Seeder;

class PantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
          {
              DB::table('pants')->insert([


                ['id' => 1, 'brand' => 'Nudie Jeans', 'name' => "Nudie Jeans Men's Thin Finn Skinny Jeans - Dry Twill", 'description' => "Men’s ‘Destroyed’ black wash jeans from Helmut Lang. Crafted from pure cotton and cut for a slim fit, the midrise ‘Mr 87’ jeans comprise a button closure above a concealed zip fastening and a classic five pocket construction.", 'material' => '100% Cotton', 'colour' => 'Black', 'pants_fit' => 'Slim Fit', 'url' => '/img/pants/nudie_jeans_mens_thin_finn_skinny_jeans.jpg', 'price' => '432.00', 'outfit' => '1'],


                ['id' => 2, 'brand' => 'Helmut Lang', 'name' => "Helmut Lang Men's Mr 87 Destroyed Jeans - Black", 'description' => "Men’s ‘Destroyed’ black wash jeans from Helmut Lang. Crafted from pure cotton and cut for a slim fit, the midrise ‘Mr 87’ jeans comprise a button closure above a concealed zip fastening and a classic five pocket construction. Featuring multiple belt loops and metal rivets.", 'material' => '100% Cotton', 'colour' => 'Black', 'pants_fit' => 'Slim Fit', 'url' => '/img/pants/helmut_lang_mens_mr_87_destroyed_jeans.jpg', 'price' => '342.00', 'outfit' => '2'],

                ['id' => 3, 'brand' => 'Levis', 'name' => "Levi's Men's 511 Slim Fit Jeans - Harbour", 'description' => "Levi's ‘511’ blue jeans with a subtle faded finish and a slim but not skin tight, tapered leg. Crafted from heavyweight 12oz denim, the stretchy five pocket jeans are fastened with a top button and zip fly and feature the signature stitched Levi's arcuates on the rear pockets. Complete with the classic leather waistband tab, exposed metal rivets and the iconic red Levi's brand tab on the rear pocket. - A.D.", 'material' => '99% Cotton, 1% Elastane', 'colour' => 'Blue', 'pants_fit' => 'Slim Fit', 'url' => '/img/pants/levis_mens_511_slim_fit_jeans.jpg', 'price' => '543.00', 'outfit' => '3'],

                ['id' => 4, 'brand' => 'rag & bone', 'name' => "rag & bone Men's Fit 1 Tapered Jeans - Black", 'description' => "The men's rag & bone Standard Issue Fit 1 Low Rise Skinny Fit Jeans have a classic five-pocket design, a buttoned fly and belt loops. Made of stretchy cotton, the black skinny jeans have a low rise. An embroidered 'rb' logo adorns the right back pocket. - L.M.", 'material' => '98% Cotton, 2% Polyurethane', 'colour' => 'Black', 'pants_fit' => 'Skinny Fit', 'url' => '/img/pants/ragandbone_mens_fit_1_tapered_jeans.jpg', 'price' => '53.00', 'outfit' => '4'],

                ['id' => 5, 'brand' => 'Versace Collection', 'name' => "Versace Collection Men's All Over Print Jeans - Black", 'description' => "Men’s black jeans with subtle print from Versace Collection. Crafted from stretch cotton and cut for a slim fit, the mid-rise denim jeans comprise a button fastening above a zip fly and multiple belt loops.", 'material' => '98% Cotton, 2% Elastane', 'colour' => 'Black', 'pants_fit' => 'Skinny Fit', 'url' => '/img/pants/versace_collection_mens_all_over_print_jeans.jpg', 'price' => '235.00', 'outfit' => '5'],

                ['id' => 6, 'brand' => 'A.P.C.', 'name' => "A.P.C. Men's Petit Standard Mid Rise Jeans - Selvedge Indigo", 'description' => "The Petit Standard jeans from A.P.C. boast a mid rise and five pockets throughout. A slim fit, indigo blue option, the jeans are finished with a button fly and belt loops. - E.D", 'material' => '100% Cotton', 'colour' => 'Indigo', 'pants_fit' => 'Skinny Fit', 'url' => '/img/pants/apc_mens_petit_standard_mid_rise_jeans.jpg', 'price' => '235.00', 'outfit' => '6'],
              ]);
          }
      }
